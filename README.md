ibox
=========

Orchestration Role for iBox Roles.

Requirements
------------

Homebrew (https://brew.sh/) must be installed.

Role Variables
--------------

| Variable                              | Required | Description                                                                       | Default |
|---------------------------------------|----------|-----------------------------------------------------------------------------------|---------|
| ibox_brew_packages_present            | false    | List of package to install via homebrew.                                          | []      |
| ibox_brew_packages_absent             | false    | List of package which will be removed via homebrew.                               | []      |
| ibox_brew_casks_present               | false    | List of casks to installed via homebrew.                                          | []      |
| ibox_brew_casks_absent                | false    | List of casks which will be removed via homebrew.                                 | []      |
| ibox_ca_cert_internal_ca_urls         | false    | List of urls to import into the keychain specified by ibox_ca_cert_macos_keychain | []      |
| ibox_ca_cert_macos_keychain           | false    | Keychain for import, only for MacOS.                                              | user    |
| ibox_ca_cert_validate_internal_ca_url | false    | Validate TLS cert for urls in ibox_ca_cert_internal_ca_urls.                      | true    |
| ibox_osx_settings                     | false    | List of macOS settings.                                                           | []      |

Dependencies
------------

Please see requirements.yml.

Example Playbook
----------------

Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.ibox.git
    version: stable
    scm: git
    name: ibox.ibox
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.ibox"
      ansible.builtin.include_role:
        name: "ibox.ibox"
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
